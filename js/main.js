$(document).ready(function () {
  $('.cart_table tr:last').addClass('last');
  $('.check').iCheck({
    checkboxClass: 'icheckbox_minimal',
    radioClass: 'iradio_minimal',
    increaseArea: '20%' // optional
  });
});